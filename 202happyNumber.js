const isHappy = (n) => {
	if (n === null || n < 1) return false;
	const thisSet = new Set();
	while (n != 1) {
		if (!thisSet.add(n)) {
			return false;
		}
		n = String(n);
		let newNum = '0';
		for (let i of n) {
			newNum = parseInt(newNum, 10) + parseInt(i ** 2, 10);
		}
		n = newNum;
	}
	return true;
};
