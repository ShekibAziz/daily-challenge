const isPowerOfTwo = (n) => {
	if (n === 1) return true;
	if (n % 2 === 1 || n === 0 || n < 0) return false;
	let rem = 0;
	while (true) {
		let rem = n % 2;
		n /= 2;
		if (n < 2 && rem > 0) return false;
		else if (n < 2 && rem === 0) return true;
	}
};

const isPowerOfTwo = (n) => {
	n > 0 ? !(n & (n - 1)) : false;
};
