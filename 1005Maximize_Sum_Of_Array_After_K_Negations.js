const largestSumAfterKNegations = (A, K) => {
	A = A.sort((a, b) => a - b);
	let i = 0;

	while (K > 0) {
		if (A[i] === 0 || (K % 2 === 0 && A[i] > 0)) break;

		A[i] = -A[i];
		if (A[i] > A[i + 1]) i++;
		K--;
	}
	return A.reduce((a, b) => a + b);
};
