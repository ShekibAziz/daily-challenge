const judgeCircle = (moves) => {
	const result = moves.split('').reduce((acc, val) => {
		acc[val] += 1;
		return acc;
	}, { U: 0, L: 0, R: 0, D: 0 });

	return result.U === result.D && result.L === result.R ? true : false;
};

const judgeCircle = (moves) => {
	let x = (y = 0);

	for (let letter of moves) {
		switch (letter) {
			case 'U':
				x++;
				break;
			case 'D':
				x--;
				break;

			case 'L':
				y--;
				break;
			case 'R':
				y++;
				break;
		}
	}
	return x === 0 && y === 0 ? true : false;
};
