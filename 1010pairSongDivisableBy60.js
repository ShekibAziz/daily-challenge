const numPairsDivisibleBy60 = (time) => {
	const arr = new Array(60).fill(0);

	let a = time.reduce((acc, each, i) => {
		acc += arr[each % 60];
		arr[(60 - each % 60) % 60] += 1;

		return acc;
	}, 0);
	return a;
};
